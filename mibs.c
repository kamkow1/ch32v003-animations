#define MIBS_IMPL
#include "mibs.h"

Mibs_Default_Allocator allocator = mibs_make_default_allocator();

typedef enum {
    CMD_BUILD,
    CMD_FLASH,
    CMD_CONFIGURE,
    CMD_CLEAN,
} Command;
static const char* string_commands[] = {"build", "flash", "configure", "clean"};

void help(void)
{
    mibs_log(MIBS_LL_INFO, "Available commands:\n");
    for (size_t i = 0; i < mibs_array_len(string_commands); i++) {
        mibs_log(MIBS_LL_INFO, "    -C=%s\n", string_commands[i]);
    }
}

#define CONFIG_PATH "build_config.h"

#ifdef CONFIGURED

#include CONFIG_PATH

bool build(void) {
    Mibs_Cmd cmd = {0};

    char* path = "ch32v003_animations.c";

    // C -> ELF
    char* filename_elf = path;
    char* ext_c = ".c";
    char* ext_elf = ".elf";
    filename_elf = mibs_string_replace(&allocator, filename_elf, ext_c, ext_elf);
    mibs_cmd_append(&allocator, &cmd, XGCC);
    mibs_cmd_append(&allocator, &cmd, path);
    mibs_cmd_append(&allocator, &cmd, "./ch32v003fun/ch32v003fun/ch32v003fun.c");
    mibs_cmd_append(&allocator, &cmd, "-o", filename_elf);
    mibs_cmd_append(&allocator, &cmd,
        "-g",
        "-ffunction-sections",
        "-static-libgcc",
        "-march=rv32ec",
        "-mabi=ilp32e",
        "-nostdlib", "-I.", "-I./ch32v003fun/ch32v003fun",
        "-Wall", "-Wextra"
    );
    mibs_cmd_append(&allocator, &cmd, "-T./ch32v003fun/ch32v003fun/ch32v003fun.ld", "-Wl,--gc-sections", "-L./ch32v003fun/misc/", "-lgcc");
    bool result = mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok;
    mibs_da_deinit(&allocator, &cmd);

    if (!result) return false;
    
    // ELF -> BIN
    char* filename_bin = path;
    char* ext_bin = ".bin";
    filename_bin = mibs_string_replace(&allocator, filename_bin, ext_c, ext_bin);
    mibs_cmd_append(&allocator, &cmd, XOBJCOPY);
    mibs_cmd_append(&allocator, &cmd, "-O", "binary", filename_elf, filename_bin);
    result = mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok;
    mibs_da_deinit(&allocator, &cmd);

    if (!result) return false;

    mibs_free(&allocator, filename_elf);
    mibs_free(&allocator, filename_bin);
    return result;
}

#define WLINK "wlink"

bool flash(const char* bin, int addr)
{
    char addrbuf[0xff];
    memset(addrbuf, 0, sizeof(addrbuf));
    sprintf(addrbuf, "0x0%x", addr);
    Mibs_Cmd cmd = {0};
    mibs_cmd_append(&allocator, &cmd, WLINK);
    mibs_cmd_append(&allocator, &cmd, "flash", "--address", addrbuf, bin);

    return mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok;
}

bool clean(void)
{
    Mibs_Cmd cmd = {0};
    mibs_cmd_append(&allocator, &cmd, "rm", "-f");

    Mibs_Read_File_Result rf_result = mibs_read_file(&allocator, "./.gitignore");
    if (!rf_result.ok) return false;

    Mibs_String_Builder file_sb = rf_result.value;
   
    Mibs_String_Array lines = mibs_split_cstr(&allocator, file_sb.items, "\n");
    for (size_t i = 0; i < lines.count; i++) {
        mibs_da_append(&allocator, &cmd, lines.items[i]);
    }

    if (!mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok) return false;

    mibs_da_deinit(&allocator, &file_sb);
    mibs_da_deinit(&allocator, &cmd);
    return true;
}

int main(int argc, char **argv)
{
    mibs_rebuild(&allocator, argc, argv);

    Mibs_Options options = mibs_options(&allocator, argc, argv);
    Mibs_Option_Value *command_opt = map_get(&options, "C");
    if (!command_opt) {
        mibs_log(MIBS_LL_ERROR, "No command provided\n");
        help();
        return 1;
    }
    if (!mibs_expect_option_kind(command_opt, "-C", MIBS_OV_STRING)) return 1;
    const char* command = command_opt->data.string;

    if (mibs_compare_cstr(command, string_commands[CMD_BUILD])) {
        if (!build()) return 1;
    } else if (mibs_compare_cstr(command, string_commands[CMD_CLEAN])) {
        if (!clean()) return 1;
    } else if (mibs_compare_cstr(command, string_commands[CMD_FLASH])) {
        Mibs_Option_Value *binary_opt = map_get(&options, "bin");
        if (!binary_opt) {
            mibs_log(MIBS_LL_ERROR, "No binary binary provided\n");
            return 1;
        }
        if (!mibs_expect_option_kind(binary_opt, "-bin", MIBS_OV_STRING)) return 1;
        const char* binary = binary_opt->data.string;

        int address = 0x08000000;

        Mibs_Option_Value *address_opt = map_get(&options, "addr");
        if (address_opt) {
            if (!mibs_expect_option_kind(address_opt, "-addr", MIBS_OV_STRING)) return 1;
            address = atoi(address_opt->data.string);
        }

        if (!flash(binary, address)) return 1;
    } else {
        mibs_log(MIBS_LL_ERROR, "No such command as %s\n", command);
        return 1;
    }

    map_deinit(&options);
    return 0;
}

#else

void generate_config_define(Mibs_String_Builder *config, const char* macro, const char* value)
{
    mibs_sb_append_cstr(&allocator, config, "#define");
    mibs_sb_append_cstr(&allocator, config, " ");
    mibs_sb_append_cstr(&allocator, config, macro);
    mibs_sb_append_cstr(&allocator, config, " ");
    mibs_sb_append_cstr(&allocator, config, value);
    mibs_sb_append_char(&allocator, config, MIBS_NL[0]);
}

int main(int argc, char** argv)
{
    mibs_rebuild(&allocator, argc, argv);

    Mibs_Options options = mibs_options(&allocator, argc, argv);
    Mibs_Option_Value *command_opt = map_get(&options, "C");
    if (!command_opt) {
        mibs_log(MIBS_LL_ERROR, "No command provided\n");
        help();
        return 1;
    }
    if (!mibs_expect_option_kind(command_opt, "-C", MIBS_OV_STRING)) return 1;
    const char* command = command_opt->data.string;

    if (mibs_compare_cstr(command, "configure")) {
        mibs_clear_file(CONFIG_PATH);

        Mibs_String_Builder config = {0};

        // xprefix must be in quotes
        char* xprefix = "\"riscv64-unknown-linux-gnu-\"";
        Mibs_Option_Value *xprefix_opt = map_get(&options, "xprefix");
        if (xprefix_opt) {
            if (!mibs_expect_option_kind(xprefix_opt, "-xprefix", MIBS_OV_STRING)) return 1;
            xprefix = (char*)xprefix_opt->data.string;
        }

        generate_config_define(&config, "XPREFIX", xprefix);
        generate_config_define(&config, "XGCC", "XPREFIX \"gcc\"");
        generate_config_define(&config, "XOBJDUMP", "XPREFIX \"objdump\"");
        generate_config_define(&config, "XOBJCOPY", "XPREFIX \"objcopy\"");

        mibs_sb_append_null(&allocator, &config);

        mibs_write_file(CONFIG_PATH, config.items);
        mibs_da_deinit(&allocator, &config);

        Mibs_Cmd cmd = {0};

        const char* configured = "mibs_configured";
        mibs_cmd_append(&allocator, &cmd, MIBS_CC, "-o", configured, __FILE__, "-DCONFIGURED");
        if (!mibs_run_cmd(&allocator, &cmd, MIBS_CMD_SYNC, NULL).ok) return 1;
    } else {
        mibs_log(MIBS_LL_ERROR, "No such command as %s\n", command);
        mibs_log(MIBS_LL_INFO, "Hint: the build system is unconfigured yet, so only -C=configure is available\n");
        return 1;
    }

    return 0;
}

#endif // CONFIGURED
