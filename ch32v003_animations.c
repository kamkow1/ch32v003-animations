#define SSD1306_128X32

#include <stdbool.h>

#include "ch32v003fun.h"
#include "ssd1306_i2c.h"
#include "ssd1306.h"

int main(void)
{
    SystemInit();
	Delay_Ms(100);

    if (!ssd1306_i2c_init()) {
        ssd1306_init();
        while(true) {
            ssd1306_setbuf(0);

            ssd1306_drawstr(0, 0, "hello world!", 1);

			ssd1306_refresh();
			Delay_Ms(2000);
        }
    }

    while(true);
}
