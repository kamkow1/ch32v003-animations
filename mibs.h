#ifndef MIBS_H_
#define MIBS_H_

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>
#include <limits.h>
#include <ctype.h>
#include <stddef.h>
#include <inttypes.h>

#if defined(unix) || defined(__unix) || defined(__unix__)
#   define MIBS_PLATFORM_UNIX
#   include <unistd.h>
#   include <dirent.h>
#   include <sys/types.h>
#   include <sys/stat.h>
#   include <sys/wait.h>
#   define MIBS_NL "\n"
#elif defined(WIN32) || defined(WIN64) || defined(_WIN32)
#   define MIBS_PLATFORM_WINDOWS
#   define WIN32_LEAN_AND_MEAN
#   define _WINUSER_
#   define _IMM_
#   define _WINCON_
#   define NOGDI
#   define NOUSER
#   include <windows.h>
#   undef near
#   undef far
#   include <io.h>
/* #   include <tchar.h> */
/* #   include <strsafe.h> */
#   define MIBS_NL "\r\n"
#endif

#ifndef MIBS_CC
#   define MIBS_CC "cc"
#endif

#define MIBS_ASSERT  assert
#define MIBS_MALLOC  malloc
#define MIBS_REALLOC realloc
#define MIBS_FREE    free
#define MIBS_LOG_STREAM stdout

// gets a piece of C code as text (literally)
#define mibs_text(x) #x

#define Mibs_Result(TValue) struct { bool ok; TValue value; }

// alignment helpers
#define mibs_align_by(N, M) (M * ((N+M-1)/M))
#define mibs_align_by_power(N, M) \
    ({ \
        int tmp = 1; \
        while(tmp < N) tmp *= M; \
        tmp; \
    })

// types for allocator handlers
#define Mibs_Allocator_Alloc_Func(T, _name) void* (*_name)(struct Mibs_Allocator_##T *self, size_t size)
#define Mibs_Allocator_Free_Func(T, _name) void (*_name)(struct Mibs_Allocator_##T *self, void* mem)
#define Mibs_Allocator_Realloc_Func(T, _name) void* (*_name)(struct Mibs_Allocator_##T *self, void* mem, size_t prev_size, size_t new_size)

// Allocator Base
// every allocator must conform to a signle interface: must provide functions 'alloc_func', 'free_func' and 'realloc_func'.
// if a structure provides fields that point to these functions, it can considered an allocator.
// NOTE 1: the default fallback allocator of each allocator is 'MIBS_MALLOC', 'MIBS_FREE', 'MIBS_REALLOC'.
// if you'd like to use a different fallback allocator, simply #define these functions before including mibs.h.
// alternatively, you can make your own allocator that will call another allocator under the hood.
// NOTE 2: all allocators should be easily swappable and usable via the 'mibs_malloc', 'mibs_free', 'mibs_realloc' interface.
// if an allocator can't free memory, simply leave the free handler implementation empty. the user should
// add extra code, if needed, when using your allocator, but shouldn't remove exising code. an allocator can log an internal error
// if it happens, but shouldn't exit - let the user decide what to do with the error.
#define Mibs_Allocator_Base(TUnderlying) \
    struct Mibs_Allocator_Base_##TUnderlying { \
        Mibs_Allocator_Alloc_Func(TUnderlying, alloc_func); \
        Mibs_Allocator_Free_Func(TUnderlying, free_func); \
        Mibs_Allocator_Realloc_Func(TUnderlying, realloc_func); \
    }

// Allocator
// An allocator must consist of two fields: 'base' and 'underlying'.
// base is the allocator's base defined by Mibs_Allocator_Base macro and provides the allocator's primitive functionallity
// the underlying type can be any type. it's used to store additional allocator information, for eg. a cursor or a buffer.
#define Mibs_Allocator(TUnderlying) \
    struct Mibs_Allocator_##TUnderlying { \
        Mibs_Allocator_Base(TUnderlying) base; \
        TUnderlying underlying; \
    }

// Fixed Buffer Allocator
// this allocator works within a fixed memory buffer. it doesn't deallocate the buffer, until the last free handler
// is called - this is done via the 'allocation_count' field. the buffer can allocate the memory itself, but it's also
// useful to utilize a preallocated buffer.

typedef struct {
    size_t cursor;
    size_t max;
    size_t allocation_count;
    void* buffer;
} Mibs_Fixed_Buffer_Underlying;

typedef Mibs_Allocator(Mibs_Fixed_Buffer_Underlying) Mibs_Fixed_Buffer_Allocator;

#define mibs_make_fixed_buffer_allocator(_buffer, _max) \
    ({ \
        Mibs_Fixed_Buffer_Allocator fb = {0}; \
        Mibs_Fixed_Buffer_Underlying fb_underlying = (Mibs_Fixed_Buffer_Underlying){ \
            .max = _max, \
            .allocation_count = _buffer != NULL ? 1 : 0, \
            .buffer = _buffer \
        }; \
        fb.underlying = fb_underlying; \
        fb.base.alloc_func = mibs_fixed_buffer_alloc; \
        fb.base.free_func = mibs_fixed_buffer_free; \
        fb.base.realloc_func = mibs_fixed_buffer_realloc; \
        fb; \
    })

#define mibs_make_fixed_buffer_allocator_owned(_buffer, _max) \
    ({ \
        Mibs_Fixed_Buffer_Allocator fb = {0}; \
        Mibs_Fixed_Buffer_Underlying fb_underlying = (Mibs_Fixed_Buffer_Underlying){ \
            .max = _max, \
            .allocation_count = 0, \
            .buffer = _buffer \
        }; \
        fb.underlying = fb_underlying; \
        fb.base.alloc_func = mibs_fixed_buffer_alloc; \
        fb.base.free_func = mibs_fixed_buffer_free; \
        fb.base.realloc_func = mibs_fixed_buffer_realloc; \
        fb; \
    })

void* mibs_fixed_buffer_alloc(Mibs_Fixed_Buffer_Allocator* fb, size_t size);
void mibs_fixed_buffer_free(Mibs_Fixed_Buffer_Allocator* fb, void* mem);
void* mibs_fixed_buffer_realloc(Mibs_Fixed_Buffer_Allocator* fb, void* mem, size_t prev_size, size_t new_size);

// Default Allocator
// the default allocator, is very simple. it just calls 'MIBS_MALLOC', 'MIBS_FREE' and 'MIBS_REALLOC' functions
// under the hood. it's just wraps these functions into the Allocator interface.
// this is useful if your build program doesn't use a specialized allocator yet, but you need to call
// for eg. 'mibs_rebuild' or 'mibs_da_append' or any other core function/macro that requires an allocator.
// to put it simply, this is just a 'make a whatever allocator, just let me use the library' allocator

typedef Mibs_Allocator(int) Mibs_Default_Allocator;

#define mibs_make_default_allocator() \
    ((Mibs_Default_Allocator) { \
        .base = { \
            .alloc_func = mibs_default_alloc, \
            .free_func = mibs_default_free, \
            .realloc_func = mibs_default_realloc \
        } \
    })

void* mibs_default_alloc(Mibs_Default_Allocator* default_alloc, size_t size);
void mibs_default_free(Mibs_Default_Allocator* default_alloc, void* mem);
void* mibs_default_realloc(Mibs_Default_Allocator* default_alloc, void* mem, size_t prev_size, size_t new_size);

// Linear Allocator
// the linear allocator can allocate as much memory as you want, but you cannot free it (in bits).
// the allocator provides an empty free function implementation, so nothing happens when you free memory,
// you have to explicity call 'mibs_linear_destroy()' to free the buffer all at once.
// this is useful if you'd like to speed up your program, by saving up on 'free()' calls, which are quite expensive.
// you can reuse the buffer without calling 'mibs_linear_destroy()' with 'mibs_linear_reset()',
// which simply resets the cursor, making it point to the beginning of the buffer
// and resets the capacity, so the allocator thinks that the buffer is clear and just has been allocated.

typedef struct {
    size_t cursor;
    size_t cap;
    void *buffer;
} Mibs_Linear_Underlying;

typedef Mibs_Allocator(Mibs_Linear_Underlying) Mibs_Linear_Allocator;

#define MIBS_ARENA_DEFAULT_CAP 256

#define mibs_make_linear_allocator() \
    ((Mibs_Linear_Allocator) { \
        .underlying = { \
            .cursor = 0, \
            .cap = MIBS_ARENA_DEFAULT_CAP, \
            .buffer = NULL \
        }, \
        .base = { \
            .alloc_func = mibs_linear_alloc, \
            .free_func = mibs_linear_free, \
            .realloc_func = mibs_linear_realloc \
        } \
    })

void* mibs_linear_alloc(Mibs_Linear_Allocator* linear_alloc, size_t size);
void mibs_linear_free(Mibs_Linear_Allocator* linear_alloc, void* mem);
void* mibs_linear_realloc(Mibs_Linear_Allocator* linear_alloc, void* mem, size_t prev_size, size_t new_size);
void mibs_linear_destroy(Mibs_Linear_Allocator* linear_alloc);
void mibs_linear_reset(Mibs_Linear_Allocator* linear_alloc);

// Allocation helper functions
// these functions are simple wrappers for calling an allocator's handlers

#define mibs_alloc(alloc, size) ((alloc)->base.alloc_func(alloc, size))
#define mibs_free(alloc, mem) ((alloc)->base.free_func(alloc, mem))
#define mibs_realloc(alloc, mem, prevsz, newsz) ((alloc)->base.realloc_func(alloc, mem, prevsz, newsz))

struct map_node_t;
typedef struct map_node_t map_node_t;

typedef struct {
  map_node_t **buckets;
  unsigned nbuckets, nnodes;
} map_base_t;

typedef struct {
  unsigned bucketidx;
  map_node_t *node;
} map_iter_t;


#define map_t(T)\
  struct { map_base_t base; T *ref; T tmp; }


#define map_init(m)\
  memset(m, 0, sizeof(*(m)))


#define map_deinit(m)\
  map_deinit_(&(m)->base)


#define map_get(m, key)\
  ( (m)->ref = map_get_(&(m)->base, key) )


#define map_set(m, key, value)\
  ( (m)->tmp = (value),\
    map_set_(&(m)->base, key, &(m)->tmp, sizeof((m)->tmp)) )


#define map_remove(m, key)\
  map_remove_(&(m)->base, key)


#define map_iter(m)\
  map_iter_()


#define map_next(m, iter)\
  map_next_(&(m)->base, iter)


void map_deinit_(map_base_t *m);
void *map_get_(map_base_t *m, const char *key);
int map_set_(map_base_t *m, const char *key, void *value, int vsize);
void map_remove_(map_base_t *m, const char *key);
map_iter_t map_iter_(void);
const char *map_next_(map_base_t *m, map_iter_t *iter);

struct map_node_t {
  unsigned hash;
  void *value;
  map_node_t *next;
  /* char key[]; */
  /* char value[]; */
};

typedef map_t(void*) map_void_t;
typedef map_t(char*) map_str_t;
typedef map_t(int) map_int_t;
typedef map_t(char) map_char_t;
typedef map_t(float) map_float_t;
typedef map_t(double) map_double_t;

#define mibs_is_unsigned(x) (x >= 0 && ~x >= 0)
#define mibs_is_unsigned_type(T) ((T)0 - 1 > 0)
#define mibs_is_signed(x) (!mibs_is_unsigned((x)))
#define mibs_is_signed_type(T) (!mibs_is_unsigned_type((T)))

#define mibs_array_len(a) (sizeof(a)/sizeof(a[0]))
#define mibs_array_get(a, i) (MIBS_ASSERT(i < mibs_array_len(a)), a[i])

#define Mibs_Da(T) struct { T* items; size_t count; size_t cap; }

#define mibs_da_get(da, i) (MIBS_ASSERT((i) < (da)->count), (da)->items[i])

#define mibs_da_deinit(alloc, da) \
    do { \
    	(da)->count = 0; \
        mibs_free((alloc), (da)->items); \
    } while(0)

#define mibs_da_append(alloc, da, item) \
    do { \
        if ((da)->count == 0) { \
            (da)->items = mibs_alloc((alloc), sizeof((item))); \
            (da)->cap = 1; \
        } else { \
        	if ((da)->count == (da)->cap) { \
                size_t old_cap = (da)->cap * sizeof((item)); \
	        	(da)->cap *= 2; \
		        (da)->items = mibs_realloc((alloc), (da)->items, old_cap, (da)->cap * sizeof((item))); \
        	} \
        } \
    	(da)->items[(da)->count++] = item; \
    } while(0)

#define mibs_da_append_array(alloc, da, array) \
    do { \
        for (size_t i = 0; i < mibs_array_len((array)); i++) { \
            mibs_da_append((alloc), (da), mibs_array_get(array, i)); \
        } \
    } while(0)

#define mibs_da_append_many(alloc, da, T, ...) \
    mibs_da_append_array((alloc), (da), ((T[]){ __VA_ARGS__ }))

#define mibs_da_concat(alloc, T, da1, da2) \
    ({ \
        Mibs_Da(T) result = {0}; \
        for (size_t i = 0; i < (da1)->count; i++) mibs_da_append((alloc), (da1), mibs_da_get((da1), i)); \
        for (size_t i = 0; i < (da2)->count; i++) mibs_da_append((alloc), (da2), mibs_da_get((da2), i)); \
        result; \
    })

#define mibs_da_concat_str_items(alloc, da, delim) \
    ({ \
        size_t size = 0; \
        for (size_t i = 0; i < (da)->count; i++) size += strlen((mibs_da_get((da), i)))+1; \
        char* buf = (char*)mibs_alloc((alloc), size); \
        memset(buf, 0, size); \
        size_t offset = 0; \
        for (size_t i = 0; i < (da)->count; i++) { \
            const char* str = mibs_da_get((da), i); \
            memcpy(buf+offset, str, strlen(str)); \
            offset += strlen(str); \
            char d[2]; \
            memset(d, 0, sizeof(d)); \
            d[0] = delim; \
            d[1] = '\0'; \
            if (i < (da)->count - 1) { \
                memcpy(buf+offset, d, sizeof(d)/sizeof(d[0])); \
                offset += 1; \
            } \
        } \
        buf; \
    })

#define mibs_da_clone(alloc, T, da) \
    ({ \
        T new_da = {0}; \
        for (size_t i = 0; i < (da)->count; i++) mibs_da_append((alloc), &new_da, mibs_da_get((da), i)); \
        new_da; \
    })

#define mibs_da_remove(da, index) \
    do { \
        for (size_t j = index; j < (da)->count; j++) { \
            (da)->items[j] = (da)->items[j+1]; \
        } \
        (da)->count -= 1; \
    } while(0)

#define mibs_copy_string(dest, src) (strcpy(dest, src));

// Da derivative types
typedef Mibs_Da(char) Mibs_DString;
typedef Mibs_Da(char) Mibs_String_Builder;
typedef Mibs_Da(Mibs_DString) Mibs_DString_Builder;
typedef Mibs_Da(const char*) Mibs_String_Array;

#define mibs_sb_append_cstr(alloc, sb, s) \
do { \
    const char* c = (char*)s; \
    while(*c) mibs_da_append((alloc), (sb), *c++); \
} while(0)

#define mibs_sb_append_char(alloc, sb, c) mibs_da_append((alloc), (sb), (c))
#define mibs_sb_append_null(alloc, sb) mibs_da_append((alloc), (sb), '\0')

#define mibs_split_cstr(alloc, s, delims) \
({ \
    Mibs_String_Array sa = {0}; \
    char* ch = strtok((char*)(s), (char*)(delims)); \
    while (ch != NULL) { \
        mibs_da_append((alloc), &sa, ch); \
        ch = strtok(NULL, (char*)(delims)); \
    } \
    sa; \
})

#define mibs_ds_append_null(alloc, ds) mibs_da_append((alloc), (ds), '\0')

#define mibs_join_sa(alloc, sa, delim) \
({ \
    Mibs_DString ds = {0}; \
    for (size_t i = 0; i < (sa)->count; i++) { \
        const char* c = (sa)->items[i]; \
        while(*c) mibs_da_append((alloc), &ds, *c++); \
        if (i < sa->count - 1) { \
            mibs_da_append((alloc), &ds, (delim)); \
        } \
    } \
    mibs_ds_append_null((alloc), &ds); \
    ds; \
})

#define mibs_ds_from_cstr(alloc, s) \
({ \
    Mibs_DString ds = {0}; \
    const char* c = (char*)(s); \
    while(*c) mibs_da_append((alloc), &ds, *c++); \
    mibs_ds_append_null((alloc), &ds); \
    ds; \
})

// NOTE: caller has to free the result buffer
#define mibs_string_replace(alloc, orig, rep, with) \
({ \
    char* result = 0; \
    char* ins = 0; \
    char* tmp = 0; \
    size_t len_rep = 0; \
    size_t len_with = 0; \
    size_t len_front = 0; \
    size_t count; \
    \
    if (!(orig) || !(rep)) { \
        result = 0; \
    } else { \
        len_rep = strlen((rep)); \
        if (len_rep == 0) { \
            result = 0; \
        } else { \
            if (!(with)) { \
                with = ""; \
            } \
            len_with = strlen((with)); \
            \
            ins = (orig); \
            for (count = 0; tmp = strstr((ins), (rep)); ++count) { \
                ins = tmp + len_rep; \
            } \
            tmp = result = mibs_alloc((alloc), strlen((orig))+(len_with-len_rep)*count+1); \
            while(count--) { \
                ins = strstr((orig), (rep)); \
                len_front = ins-(orig); \
                tmp = strncpy(tmp, orig, len_front) + len_front; \
                tmp = strcpy(tmp, with) + len_with; \
                orig += len_front + len_rep; \
            } \
            strcpy(tmp, orig); \
        } \
    } \
    \
    result; \
})

typedef enum {
    MIBS_LL_INFO,
    MIBS_LL_WARNING,
    MIBS_LL_ERROR,
    MIBS_LL_DEBUG,
} Mibs_Log_Lvl;

#define mibs_log(lvl, fmt, ...) fprintf(MIBS_LOG_STREAM, "mibs (%s): " fmt, mibs_array_get(mibs_log_lvl_prefixes, lvl), ##__VA_ARGS__)

#if defined(MIBS_PLATFORM_UNIX)
#   define MIBS_INVALID_PROCESS -1
typedef int Mibs_Process;
#elif defined(MIBS_PLATFORM_WINDOWS)
#   define MIBS_INVALID_PROCESS INVALID_HANDLE_VALUE
typedef HANDLE Mibs_Process;
#else
#   error "Unimplemented platform"
#endif

#ifdef MIBS_PLATFORM_WINDOWS
#   define UNIX_TIME_START 0x019DB1DED53E8000
#   define TICKS_PER_SEC   10000000

int64_t windows_system_time_to_unix(FILETIME ft);
int64_t windows_get_file_last_write(HANDLE hFile);
#define windows_GetLastError_as_ds(alloc) \
({ \
    Mibs_DString ds = {0}; \
    DWORD err = GetLastError(); \
    if (err != 0) { \
        \
        LPSTR message_buffer = NULL; \
        FormatMessageA( \
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, \
            NULL, \
            err, \
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), \
            (LPSTR)&message_buffer, \
            0, NULL \
        ); \
        ds = mibs_ds_from_cstr((alloc), message_buffer); \
        LocalFree(message_buffer); \
    } \
    ds; \
})
#endif

#define MIBS_CMD_SYNC  1
#define MIBS_CMD_ASYNC 0

typedef Mibs_Result(Mibs_Process) Mibs_Cmd_Result;

#define make_bad_cmd_result() ((Mibs_Cmd_Result){ .ok = false, .value = MIBS_INVALID_PROCESS })

#define mibs_log_cmd(alloc, log_lvl, cmd, cwd) \
    do { \
        Mibs_String_Builder sb = {0}; \
        char* str = mibs_da_concat_str_items((alloc), (cmd), ' '); \
        mibs_sb_append_cstr((alloc), &sb, "command "); \
        mibs_sb_append_char((alloc), &sb, '\''); \
        mibs_sb_append_cstr((alloc), &sb, str); \
        mibs_sb_append_char((alloc), &sb, '\''); \
        mibs_sb_append_cstr((alloc), &sb, " in "); \
        mibs_sb_append_char((alloc), &sb, '\''); \
        mibs_sb_append_cstr((alloc), &sb, cwd); \
        mibs_sb_append_char((alloc), &sb, '\''); \
        mibs_sb_append_null((alloc), &sb); \
        mibs_log(log_lvl, "%s" MIBS_NL, sb.items); \
        mibs_free((alloc), str); \
        mibs_da_deinit((alloc), &sb); \
    } while(0)

typedef Mibs_Da(const char*) Mibs_Cmd;

#define _windows_run_cmd_cleanup(alloc) \
    do { \
        for (size_t i = 0; i < tmp.count; i++) { \
            mibs_free((alloc), (void*)tmp.items[i]); \
        } \
        mibs_da_deinit((alloc), &tmp); \
        mibs_da_deinit((alloc), &ds); \
        mibs_free((alloc), commandline); \
    } while(0)

#if defined(MIBS_PLATFORM_UNIX)
#define mibs_run_cmd(alloc, cmd, sync, cwd) \
({ \
    Mibs_Process pid = fork(); \
    Mibs_Cmd_Result result = (Mibs_Cmd_Result){ .ok = true, .value = pid };  \
    if (pid < 0) { \
        mibs_log(MIBS_LL_ERROR, "cannot fork" MIBS_NL); \
        result = make_bad_cmd_result(); \
    } else if (pid == 0) { \
        Mibs_Cmd null_cmd = mibs_da_clone((alloc), Mibs_Cmd, (cmd)); \
        mibs_da_append((alloc), &null_cmd, (char*)NULL); \
        \
        char cwdbuf[PATH_MAX]; \
        getcwd(cwdbuf, sizeof(cwdbuf)); \
        \
        if ((cwd) != NULL) chdir((cwd)); \
        mibs_log_cmd((alloc), MIBS_LL_INFO, (cmd), (cwd) != NULL ? (cwd) : cwdbuf); \
        \
        int execvp_status = execvp((cmd)->items[0], (char* const*)null_cmd.items); \
        if (execvp_status < 0) { \
            result = make_bad_cmd_result(); \
        } \
    } else { \
        if (sync) { \
            while(true) { \
                int status; \
                if (waitpid(pid, &status, 0) < 0) { \
                    mibs_log(MIBS_LL_ERROR, "could not wait for command (%d): %s"MIBS_NL, pid, strerror(errno)); \
                    result = make_bad_cmd_result(); \
                    break; \
                } \
                \
                if (WIFSIGNALED(status)) { \
                    mibs_log(MIBS_LL_ERROR, "command was terminated by %s" MIBS_NL, strsignal(WTERMSIG(status))); \
                    result = make_bad_cmd_result(); \
                    break; \
                } \
                if (WIFEXITED(status)) { \
                    int ec = WEXITSTATUS(status); \
                    if (ec != 0) { \
                        mibs_log(MIBS_LL_ERROR, "failed to run command (%d)" MIBS_NL, ec); \
                        result = make_bad_cmd_result(); \
                    } \
                    break; \
                } \
            } \
        } \
    } \
    result; \
})
#elif defined(MIBS_PLATFORM_WINDOWS)
#define mibs_run_cmd(alloc, cmd, sync, cwd) \
({ \
    Mibs_Cmd_Result result = {0}; \
    STARTUPINFO si; \
    ZeroMemory(&si, sizeof(si)); \
    si.cb = sizeof(si); \
    si.hStdError = GetStdHandle(STD_ERROR_HANDLE); \
    si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE); \
    si.hStdInput = GetStdHandle(STD_INPUT_HANDLE); \
    si.dwFlags |= STARTF_USESTDHANDLES; \
    \
    PROCESS_INFORMATION pi; \
    ZeroMemory(&pi, sizeof(pi)); \
    \
    if (cwd != NULL) { \
        for (size_t i = 0; ((char*)cwd)[i] != '\0'; i++) { \
            if (((char*)cwd)[i] == '/') ((char*)cwd)[i] = '\\'; \
        } \
    } \
    \
    Mibs_Cmd tmp = {0}; \
    \
    for (size_t i = (cmd)->count > 1 ? 1 : 0; i < (cmd)->count; i++) { \
        char* s = (char*)mibs_alloc((alloc), strlen((cmd)->items[i])); \
        mibs_copy_string(s, (cmd)->items[i]); \
        mibs_cmd_append((alloc), &tmp, s); \
    } \
    \
    char* commandline = NULL; \
    if ((cmd)->count > 1) { commandline = mibs_da_concat_str_items((alloc), &tmp, ' '); } \
    \
    char cwdbuf[MAX_PATH]; \
    ZeroMemory(&cwdbuf, sizeof(cwdbuf)); \
    GetCurrentDirectory(MAX_PATH, cwdbuf); \
    mibs_log_cmd((alloc), MIBS_LL_INFO, (cmd), (cwd) != NULL ? (cwd) : cwdbuf); \
    BOOL ok = CreateProcess((LPCSTR)(cmd)->items[0], (LPSTR)commandline, NULL, NULL, TRUE, 0, NULL, (LPCSTR)(cwd), &si, &pi); \
    \
    if (ok) { \
        if (sync) { \
            if (WaitForSingleObject(pi.hProcess, INFINITE) == WAIT_FAILED) { \
                Mibs_DString ds = windows_GetLastError_as_ds((alloc)); \
                mibs_log(MIBS_LL_ERROR, "could not wait for command: %s"MIBS_NL, ds.items); \
                _windows_run_cmd_cleanup((alloc)); \
                result = make_bad_cmd_result(); \
            } else { \
                DWORD exitCode; \
                if (!GetExitCodeProcess(pi.hProcess, &exitCode)) { \
                    Mibs_DString ds = windows_GetLastError_as_ds((alloc)); \
                    mibs_log(MIBS_LL_ERROR, "could not get process error code: %sMIBS_NL", ds.items); \
                    _windows_run_cmd_cleanup((alloc)); \
                    result = make_bad_cmd_result(); \
                } else if (exitCode != 0) { \
                    Mibs_DString ds = windows_GetLastError_as_ds((alloc)); \
                    mibs_log(MIBS_LL_ERROR, "Failed to run command: %s"MIBS_NL, ds.items); \
                    _windows_run_cmd_cleanup((alloc)); \
                    result = make_bad_cmd_result(); \
                } else {\
                    CloseHandle(pi.hProcess); \
                    result = (Mibs_Cmd_Result){ .ok = true, .value = MIBS_INVALID_PROCESS }; \
                } \
            } \
        } else { \
            CloseHandle(pi.hThread); \
            result = (Mibs_Cmd_Result){ .ok = true, .value = pi.hProcess }; \
        } \
    } else { \
        Mibs_DString ds = windows_GetLastError_as_ds((alloc)); \
        mibs_log(MIBS_LL_ERROR, "Failed to create the process: %s"MIBS_NL, ds.items); \
        _windows_run_cmd_cleanup((alloc)); \
        result = make_bad_cmd_result(); \
    } \
    result; \
})
#else
#   error "Unimplemented platform"
#endif
#define mibs_cmd_append(alloc, cmd, ...) \
    mibs_da_append_many((alloc), (cmd), const char*, ##__VA_ARGS__)

#if defined(MIBS_PLATFORM_UNIX)
#define MIBS_PATH_SEP "/"
#elif defined(MIBS_PLATFORM_WINDOWS)
#define MIBS_PATH_SEP "\\"
#else
#error "Unimplemented Platform"
#endif

typedef enum {
    MIBS_FK_TEXT,
    MIBS_FK_BIN,
} Mibs_File_Kind;

typedef enum {
    MIBS_FCR_CANNOT_OPEN_SRC = -1,
    MIBS_FCR_CANNOT_OPEN_DEST = -2,
    MIBS_FCR_DIFFERENT_ERROR = -3,
    MIBS_FCR_SUCCESS = 0,
} Mibs_Fs_Copy_Result;

typedef enum {
    MIBS_FSEK_DIR,
    MIBS_FSEK_FILE,
    MIBS_FSEK_LINK,
    MIBS_FSEK_UNKNOWN,
} Mibs_Fs_Entry_Kind;
#define mibs_fsek_to_string(fsek) (mibs_fs_entry_kind_names[(fsek)])

typedef struct {
    const char* path; // has to be freed
    Mibs_Fs_Entry_Kind kind;
} Mibs_Fs_Entry;

typedef Mibs_Da(Mibs_Fs_Entry) Mibs_Fs_Entries;

typedef Mibs_Result(Mibs_Fs_Entries) Mibs_Open_Dir_Result;

#define mibs_fs_entries_free(alloc, entries) \
do { \
    for (size_t i = 0; i < (entries)->count; i++) { \
        Mibs_Fs_Entry ent = (entries)->items[i]; \
        mibs_free((alloc), (void*)ent.path); \
    } \
    mibs_da_deinit((alloc), entries); \
} while(0)

#if defined(MIBS_PLATFORM_UNIX)
#define mibs_open_dir(alloc, dir_path) \
({ \
    Mibs_Open_Dir_Result result = (Mibs_Open_Dir_Result){.ok = true, .value = {0}}; \
    DIR* dir = NULL; \
    if ((dir = opendir(dir_path)) == NULL) { \
        result.ok = false; \
        mibs_log(MIBS_LL_ERROR, "cannot open directory %s: %n"MIBS_NL, dir_path, strerror(errno)); \
    } else { \
        struct dirent* entry = NULL; \
        while((entry = readdir(dir)) != NULL) { \
            Mibs_Fs_Entry_Kind kind = 0; \
            switch(entry->d_type) { \
            case DT_DIR:     kind = MIBS_FSEK_DIR;     break; \
            case DT_REG:     kind = MIBS_FSEK_FILE;    break; \
            case DT_LNK:     kind = MIBS_FSEK_LINK;    break; \
            case DT_UNKNOWN: kind = MIBS_FSEK_UNKNOWN; break; \
            } \
            \
            char* full_name = (char*)mibs_alloc((alloc), PATH_MAX); \
            memset(full_name, 0, PATH_MAX); \
            sprintf(full_name, "%s%c%s", dir_path, MIBS_PATH_SEP[0], entry->d_name); \
            Mibs_Fs_Entry fs_entry = (Mibs_Fs_Entry){ \
                .path = full_name, \
                .kind = kind, \
            }; \
            mibs_da_append((alloc), &result.value, fs_entry); \
        } \
        closedir(dir); \
    } \
    result; \
})

#elif defined(MIBS_PLATFORM_WINDOWS)

// https://stackoverflow.com/questions/41404711/how-to-list-files-in-a-directory-using-the-windows-api
// https://learn.microsoft.com/en-us/windows/win32/fileio/file-attribute-constants
// https://learn.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-win32_find_dataa
// https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-findnextfilea
#define mibs_open_dir(alloc, dir_path) \
({ \
    Mibs_Open_Dir_Result result = (Mibs_Open_Dir_Result){.ok = true, .value = {0}}; \
    Mibs_String_Builder tmp_path_sb = {0}; \
    mibs_sb_append_cstr((alloc), &tmp_path_sb, dir_path); \
    mibs_sb_append_cstr((alloc), &tmp_path_sb, "\\*"); \
    \
    WIN32_FIND_DATAA findData; \
    HANDLE hFind = INVALID_HANDLE_VALUE; \
    \
    hFind = FindFirstFileA(tmp_path_sb.items, &findData); \
    if (hFind == INVALID_HANDLE_VALUE) { \
        result.ok = false; \
        mibs_log(MIBS_LL_ERROR, "cannot open directory %s: %n"MIBS_NL, dir_path, strerror(errno)); \
    } else { \
        while(FindNextFileA(hFind, &findData) != 0) { \
            char* full_name = (char*)mibs_alloc((alloc), MAX_PATH); \
            memset(full_name, 0, MAX_PATH); \
            sprintf(full_name, "%s%c%s", dir_path, MIBS_PATH_SEP[0], findData.cFileName); \
            \
            Mibs_Fs_Entry_Kind kind = 0; \
            if (findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) { \
                kind = MIBS_FSEK_DIR; \
            } else if (findData.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT) { \
                kind = MIBS_FSEK_LINK; \
            } else { \
                kind = MIBS_FSEK_FILE; \
            } \
            \
            Mibs_Fs_Entry fs_entry = (Mibs_Fs_Entry){ \
                .path = full_name, \
                .kind = kind, \
            }; \
            mibs_da_append((alloc), &result.value, fs_entry); \
        } \
        FindClose(hFind); \
    } \
    result; \
})

#else
#error "Unimplemented Platform"
#endif

Mibs_Fs_Copy_Result mibs_fs_copy(const char* src, const char* dest, Mibs_File_Kind file_kind);
bool mibs_fs_rename(const char* oldpath, const char *newpath);
const char* mibs_get_file_ext(const char* file);
const char* mibs_fs_entry_kind_to_string(Mibs_Fs_Entry_Kind fsek);
bool mibs_create_file(const char* path);
bool mibs_write_file(const char* path, const char* text);
bool mibs_write_file_bin(const char* path, const char* bytes, size_t len);
bool mibs_file_exists(const char* path);
bool mibs_clear_file(const char* path);

#define MIBS_LINE_LEN 0xff
typedef Mibs_Result(Mibs_String_Builder) Mibs_Read_File_Result;

#define mibs_read_file(alloc, file_path) \
    ({ \
        Mibs_Read_File_Result result = {0}; \
        FILE* fp; \
        fp = fopen(file_path, "r"); \
        if (fp == NULL) { \
            mibs_log(MIBS_LL_ERROR, "Failed to read file: %s\n", file_path); \
            result.ok = false; \
        } else { \
            Mibs_String_Builder sb = {0}; \
            char buffer[MIBS_LINE_LEN]; \
            memset(buffer, 0, sizeof(buffer)); \
            while(fgets(buffer, MIBS_LINE_LEN, fp)) { \
                mibs_sb_append_cstr((alloc), &sb, buffer); \
            } \
            mibs_sb_append_null((alloc), &sb); \
            fclose(fp); \
            result.ok = true; \
            result.value = sb; \
        } \
        result; \
    })

int mibs_needs_rebuild(const char* out, const char* in);

#define mibs_needs_rebuild_many(bin, ...) \
    ({ \
        bool result = false; \
        const char* files[] = { __VA_ARGS__ }; \
        size_t len = sizeof(files)/sizeof(files[0]); \
        for (size_t i = 0; i < len; i++) { \
            if (mibs_needs_rebuild(files[i], bin)) { \
                result = true; \
            } \
        } \
        result; \
    })

#if defined(MIBS_PLATFORM_UNIX)
#define _mibs_rebuild_old_name(alloc, bin) \
    do { \
        mibs_sb_append_cstr((alloc), &sb, (bin)); \
        mibs_sb_append_cstr((alloc), &sb, "_old"); \
        mibs_sb_append_null((alloc), &sb); \
    } while(0)
#elif defined(MIBS_PLATFORM_WINDOWS)
// the tmpds.count - ... numbers mean .exe in reverse
#define _mibs_rebuild_old_name(alloc, bin) \
    do { \
        Mibs_DString tmpds = mibs_ds_from_cstr((alloc), (bin)); \
        mibs_da_remove(&tmpds, tmpds.count-1); \
        mibs_da_remove(&tmpds, tmpds.count-2); \
        mibs_da_remove(&tmpds, tmpds.count-3); \
        mibs_da_remove(&tmpds, tmpds.count-4); \
        mibs_sb_append_cstr((alloc), &sb, bin); \
        mibs_sb_append_cstr((alloc), &sb, "_old"); \
        mibs_sb_append_cstr((alloc), &sb, ".exe"); \
        mibs_sb_append_null((alloc), &sb); \
    } while(0)
#endif

#define mibs_rebuild(alloc, argc, argv) \
    do { \
        const char* bin = argv[0]; \
        if (mibs_needs_rebuild(bin, __FILE__)) { \
            Mibs_String_Builder sb = {0}; \
            _mibs_rebuild_old_name((alloc), bin); \
            if (!mibs_fs_rename(bin, sb.items)) exit(1); \
            Mibs_Cmd build_cmd = {0}; \
            mibs_da_append((alloc), &build_cmd, MIBS_CC); \
            mibs_da_append_many((alloc), &build_cmd, const char*, "-o", bin); \
            mibs_da_append((alloc), &build_cmd, __FILE__); \
            Mibs_Cmd_Result build_result = mibs_run_cmd((alloc), &build_cmd, MIBS_CMD_SYNC, NULL); \
            mibs_da_deinit((alloc), &build_cmd); \
            if (!build_result.ok) { \
                mibs_log(MIBS_LL_ERROR, "mibs failed to recompile itself"MIBS_NL); \
                mibs_fs_rename(sb.items, bin); \
                exit(1); \
            } \
            mibs_da_deinit((alloc), &sb); \
            Mibs_Cmd run_cmd = {0}; \
            for (size_t i = 0; i < (size_t)argc; i++) mibs_da_append((alloc), &run_cmd, argv[i]); \
            Mibs_Cmd_Result cmd_result = mibs_run_cmd((alloc), &run_cmd, MIBS_CMD_SYNC, NULL); \
            if (!cmd_result.ok) exit(1); \
            mibs_da_deinit((alloc), &run_cmd); \
            exit(0); \
        } \
    } while(0)

// Simple commandline argument parser

typedef struct {
    union {
        bool boolean;
        int integer;
        const char* string;
    } data;
    enum {
        MIBS_OV_BOOLEAN, 
        MIBS_OV_INTEGER, 
        MIBS_OV_STRING, 
    } kind;
} Mibs_Option_Value;

typedef map_t(Mibs_Option_Value) Mibs_Options;

// String parsing helpers
bool mibs_check_int(const char* s);
bool mibs_compare_cstr(const char* lhs, const char* rhs);

#define MIBS_BOOL_OPTION_TRUE  "true"
#define MIBS_BOOL_OPTION_FALSE "false"

// option syntax
// program -string_option="hello" -bool_option=true|false -int_option=55
#define mibs_options(alloc, argc, argv) \
({ \
    Mibs_Options opts; \
    map_init(&opts); \
    \
    for (int i = 1; i < argc; i++) { \
        const char* option = argv[i]; \
        Mibs_DString ds = mibs_ds_from_cstr((alloc), option); \
        mibs_da_remove(&ds, 0); \
        \
        Mibs_String_Array sa = mibs_split_cstr((alloc), ds.items, "="); \
        \
        Mibs_Option_Value value = {0}; \
        \
        if (mibs_check_int(sa.items[1])) { \
            value.kind = MIBS_OV_INTEGER; \
            value.data.integer = atoi(sa.items[1]); \
        } else if (mibs_compare_cstr(sa.items[1], MIBS_BOOL_OPTION_TRUE)) { \
            value.kind = MIBS_OV_BOOLEAN; \
            value.data.boolean = true; \
        } else if (mibs_compare_cstr(sa.items[1], MIBS_BOOL_OPTION_FALSE)) { \
            value.kind = MIBS_OV_BOOLEAN; \
            value.data.boolean = false; \
        } else { \
            value.kind = MIBS_OV_STRING; \
            value.data.string = sa.items[1]; \
        } \
        \
        map_set(&opts, sa.items[0], value); \
    } \
    opts; \
})
const char* mibs_option_kind_to_cstr(int kind);
bool mibs_expect_option_kind(Mibs_Option_Value* ov, const char* key, int kind);

#ifdef MIBS_IMPL

static const char* mibs_log_lvl_prefixes[] = {"info", "warning", "error", "debug"};

// Mibs allocators

void* mibs_default_alloc(Mibs_Default_Allocator* default_alloc, size_t size) { return MIBS_MALLOC(size); }
void mibs_default_free(Mibs_Default_Allocator* default_alloc, void* mem) { MIBS_FREE(mem); }
void* mibs_default_realloc(Mibs_Default_Allocator* default_alloc, void* mem, size_t prev_size, size_t new_size)
{
    return MIBS_REALLOC(mem, new_size);
}

void* mibs_fixed_buffer_alloc(Mibs_Fixed_Buffer_Allocator* fb, size_t size)
{
    /* size_t aligned_size = mibs_align_by_power(size, 2); */
    size_t aligned_size = size;
    if (fb->underlying.buffer == NULL) {
        fb->underlying.buffer = MIBS_MALLOC(fb->underlying.max);
        fb->underlying.cursor += aligned_size;
        return fb->underlying.buffer;
    }
    if (fb->underlying.cursor + aligned_size > fb->underlying.max) {
        mibs_log(
            MIBS_LL_ERROR,
            "Fixed Buffer Allocator: Cursor out of bounds. cursor = %zu, max = %zu, size = %zu"MIBS_NL,
            fb->underlying.cursor + aligned_size, fb->underlying.max, size
        );
        return NULL;
    }
    void* prev = fb->underlying.buffer + fb->underlying.cursor;
    fb->underlying.cursor += aligned_size;
    fb->underlying.allocation_count += 1;
    return prev;
}

void mibs_fixed_buffer_free(Mibs_Fixed_Buffer_Allocator* fb, void* mem)
{
    if (fb->underlying.buffer != NULL) {
        if (fb->underlying.allocation_count == 1) {
            MIBS_FREE(fb->underlying.buffer);
            fb->underlying.buffer = NULL;
            fb->underlying.cursor = 0;
        } else {
            fb->underlying.allocation_count -= 1;
        }
    } else {
        mibs_log(MIBS_LL_ERROR, "Fixed Buffer Allocator: The buffer is empty!"MIBS_NL);
    }
}

void* mibs_fixed_buffer_realloc(Mibs_Fixed_Buffer_Allocator* fb, void* mem, size_t prev_size, size_t new_size)
{
    if (new_size == 0) { mibs_fixed_buffer_free(fb, mem); return NULL; }
    if (!mem) return mibs_fixed_buffer_alloc(fb, new_size);
    if (prev_size <= new_size) return mem;

    fb->underlying.cursor -= prev_size;
    void* new_buffer = mibs_fixed_buffer_alloc(fb, new_size);
    if (new_buffer == NULL) return new_buffer;
    memcpy(new_buffer, mem, new_size);
    mibs_fixed_buffer_free(fb, mem);
    return new_buffer;
}

void* mibs_linear_alloc(Mibs_Linear_Allocator* linear_alloc, size_t size)
{
    /* size_t aligned_size = mibs_align_by_power(size, 2); */
    size_t aligned_size = size;
    if (linear_alloc->underlying.buffer == NULL) {
        linear_alloc->underlying.buffer = MIBS_MALLOC(aligned_size);
        linear_alloc->underlying.cursor += aligned_size;
        return linear_alloc->underlying.buffer;
    }

    if (linear_alloc->underlying.cursor + aligned_size > linear_alloc->underlying.cap) {
        linear_alloc->underlying.cap *= 2;
        linear_alloc->underlying.buffer = MIBS_REALLOC(linear_alloc->underlying.buffer, linear_alloc->underlying.cap);
    }
   
    void* prev = linear_alloc->underlying.buffer + linear_alloc->underlying.cursor;
    linear_alloc->underlying.cursor += aligned_size;
    return prev;
}

void mibs_linear_free(Mibs_Linear_Allocator* linear_alloc, void* mem) { }

void* mibs_linear_realloc(Mibs_Linear_Allocator* linear_alloc, void* mem, size_t prev_size, size_t new_size)
{
    MIBS_ASSERT(new_size >= prev_size);
    void* new_mem = mibs_linear_alloc(linear_alloc, new_size);
    memcpy(new_mem, mem, new_size);
    return new_mem;
}

void mibs_linear_destroy(Mibs_Linear_Allocator* linear_alloc)
{
    MIBS_FREE(linear_alloc->underlying.buffer);
    linear_alloc->underlying.buffer = NULL;
    linear_alloc->underlying.cursor = 0;
    linear_alloc->underlying.cap = MIBS_ARENA_DEFAULT_CAP;
}

void mibs_linear_reset(Mibs_Linear_Allocator* linear_alloc) { linear_alloc->underlying.cursor = 0; }

// Map implementation: https://github.com/rxi/map

unsigned map_hash(const char *str) {
  unsigned hash = 5381;
  while (*str) {
    hash = ((hash << 5) + hash) ^ *str++;
  }
  return hash;
}


map_node_t *map_newnode(const char *key, void *value, int vsize) {
  map_node_t *node;
  int ksize = strlen(key) + 1;
  int voffset = ksize + ((sizeof(void*) - ksize) % sizeof(void*));
  node = (map_node_t*)MIBS_MALLOC(sizeof(*node) + voffset + vsize);
  if (!node) return NULL;
  memcpy(node + 1, key, ksize);
  node->hash = map_hash(key);
  node->value = ((char*) (node + 1)) + voffset;
  memcpy(node->value, value, vsize);
  return node;
}


int map_bucketidx(map_base_t *m, unsigned hash) {
  /* If the implementation is changed to allow a non-power-of-2 bucket count,
   * the line below should be changed to use mod instead of AND */
  return hash & (m->nbuckets - 1);
}


void map_addnode(map_base_t *m, map_node_t *node) {
  int n = map_bucketidx(m, node->hash);
  node->next = m->buckets[n];
  m->buckets[n] = node;
}


int map_resize(map_base_t *m, int nbuckets) {
  map_node_t *nodes, *node, *next;
  map_node_t **buckets;
  int i; 
  /* Chain all nodes together */
  nodes = NULL;
  i = m->nbuckets;
  while (i--) {
    node = (m->buckets)[i];
    while (node) {
      next = node->next;
      node->next = nodes;
      nodes = node;
      node = next;
    }
  }
  /* Reset buckets */
  buckets = (map_node_t**)MIBS_REALLOC(m->buckets, sizeof(*m->buckets) * nbuckets);
  if (buckets != NULL) {
    m->buckets = buckets;
    m->nbuckets = nbuckets;
  }
  if (m->buckets) {
    memset(m->buckets, 0, sizeof(*m->buckets) * m->nbuckets);
    /* Re-add nodes to buckets */
    node = nodes;
    while (node) {
      next = node->next;
      map_addnode(m, node);
      node = next;
    }
  }
  /* Return error code if realloc() failed */
  return (buckets == NULL) ? -1 : 0;
}


map_node_t **map_getref(map_base_t *m, const char *key) {
  unsigned hash = map_hash(key);
  map_node_t **next;
  if (m->nbuckets > 0) {
    next = &m->buckets[map_bucketidx(m, hash)];
    while (*next) {
      if ((*next)->hash == hash && !strcmp((char*) (*next + 1), key)) {
        return next;
      }
      next = &(*next)->next;
    }
  }
  return NULL;
}


void map_deinit_(map_base_t *m) {
  map_node_t *next, *node;
  int i;
  i = m->nbuckets;
  while (i--) {
    node = m->buckets[i];
    while (node) {
      next = node->next;
      MIBS_FREE(node);
      node = next;
    }
  }
  MIBS_FREE(m->buckets);
}


void *map_get_(map_base_t *m, const char *key) {
  map_node_t **next = map_getref(m, key);
  return next ? (*next)->value : NULL;
}


int map_set_(map_base_t *m, const char *key, void *value, int vsize) {
  int n, err;
  map_node_t **next, *node;
  /* Find & replace existing node */
  next = map_getref(m, key);
  if (next) {
    memcpy((*next)->value, value, vsize);
    return 0;
  }
  /* Add new node */
  node = map_newnode(key, value, vsize);
  if (node == NULL) goto fail;
  if (m->nnodes >= m->nbuckets) {
    n = (m->nbuckets > 0) ? (m->nbuckets << 1) : 1;
    err = map_resize(m, n);
    if (err) goto fail;
  }
  map_addnode(m, node);
  m->nnodes++;
  return 0;
  fail:
  if (node) MIBS_FREE(node);
  return -1;
}


void map_remove_(map_base_t *m, const char *key) {
  map_node_t *node;
  map_node_t **next = map_getref(m, key);
  if (next) {
    node = *next;
    *next = (*next)->next;
    MIBS_FREE(node);
    m->nnodes--;
  }
}


map_iter_t map_iter_(void) {
  map_iter_t iter;
  iter.bucketidx = -1;
  iter.node = NULL;
  return iter;
}


const char *map_next_(map_base_t *m, map_iter_t *iter) {
  if (iter->node) {
    iter->node = iter->node->next;
    if (iter->node == NULL) goto nextBucket;
  } else {
    nextBucket:
    do {
      if (++iter->bucketidx >= m->nbuckets) {
        return NULL;
      }
      iter->node = m->buckets[iter->bucketidx];
    } while (iter->node == NULL);
  }
  return (char*) (iter->node + 1);
}

// -1 cannot open src
// -2 cannot open dest
// -3 different error
// 0 success
Mibs_Fs_Copy_Result mibs_fs_copy(const char* src, const char* dest, Mibs_File_Kind file_kind)
{
    FILE* r_stream = fopen(src, file_kind == MIBS_FK_TEXT ? "r" : "rb");
    if (r_stream == NULL) {
        mibs_log(MIBS_LL_ERROR, "could not open file %s: %s"MIBS_NL, src, strerror(errno));
        return MIBS_FCR_CANNOT_OPEN_SRC;
    }

    FILE* w_stream = fopen(dest, file_kind == MIBS_FK_BIN ? "w" : "wb");
    if (r_stream == NULL) {
        mibs_log(MIBS_LL_ERROR, "could not open file %s: %s"MIBS_NL, dest, strerror(errno));
        return MIBS_FCR_CANNOT_OPEN_DEST;
    }

    if (file_kind == MIBS_FK_TEXT) {
        int c;
        while((c = fgetc(r_stream)) != EOF) fputc(c, w_stream);
    } else if (file_kind == MIBS_FK_BIN) {
        fseek(r_stream, 0L, SEEK_END);
        size_t file_size = ftell(r_stream);
        fseek(r_stream, 0L, SEEK_SET);

        char* buffer = (char*)MIBS_MALLOC(file_size);

        fread(buffer, file_size, 1, r_stream);
        fwrite(buffer, file_size, 1, w_stream);

        MIBS_FREE(buffer);
    } else {
        mibs_log(MIBS_LL_ERROR, "mibs_copy(): unknown file kind"MIBS_NL);
        return MIBS_FCR_DIFFERENT_ERROR;
    }
    return MIBS_FCR_SUCCESS;
}

bool mibs_fs_rename(const char* oldpath, const char *newpath)
{
    mibs_log(MIBS_LL_INFO, "renaming from \"%s\" to \"%s\""MIBS_NL, oldpath, newpath);
#if defined(MIBS_PLATFORM_UNIX)
    if (rename(oldpath, newpath) < 0) {
        return false;
    }
#endif
    return true;
}

const char* mibs_get_file_ext(const char* file)
{
    const char* ext = strrchr(file, '.');
    if (!ext) return NULL;
    return ext+1;
}

bool mibs_create_file(const char* path)
{
#if defined(MIBS_PLATFORM_UNIX)
    return mknod(path, S_IFREG | 0666, 0) != 0;
#elif defined(MIBS_PLATFORM_WINDOWS)
#   error "MIBS_PLATFORM_WINDOWS in unimplemented yet"
#else
#   error "Unknown platform"
#endif
}

bool mibs_write_file(const char* path, const char* text)
{
#if defined(MIBS_PLATFORM_UNIX)
    FILE* file = fopen(path, "a");
    if (file == NULL) return false;
    fputs(text, file);
    fclose(file);
    return true;
#elif defined(MIBS_PLATFORM_WINDOWS)
#   error "MIBS_PLATFORM_WINDOWS in unimplemented yet"
#else
#   error "Unknown platform"
#endif
}

bool mibs_write_file_bin(const char* path, const char* bytes, size_t len)
{
#if defined(MIBS_PLATFORM_UNIX)
    FILE* file = fopen(path, "ab");
    if (file == NULL) return false;
    fwrite(bytes, 1, len, file);
    fclose(file);
    return true;
#elif defined(MIBS_PLATFORM_WINDOWS)
#   error "MIBS_PLATFORM_WINDOWS in unimplemented yet"
#else
#   error "Unknown platform"
#endif
}

bool mibs_file_exists(const char* path)
{
#if defined(MIBS_PLATFORM_UNIX)
    struct stat statbuf;
    stat(path, &statbuf);
    return statbuf.st_mode & F_OK;
#elif defined(MIBS_PLATFORM_WINDOWS)
#   error "MIBS_PLATFORM_WINDOWS in unimplemented yet"
#else
#   error "Unknown platform"
#endif
}

bool mibs_clear_file(const char* path)
{
#if defined(MIBS_PLATFORM_UNIX)
    fclose(fopen(path, "w"));
#elif defined(MIBS_PLATFORM_WINDOWS)
#   error "MIBS_PLATFORM_WINDOWS in unimplemented yet"
#else
#   error "Unknown platform"
#endif
}

static const char* mibs_fs_entry_kind_names[] = {
    [MIBS_FSEK_DIR]     = "Directory",
    [MIBS_FSEK_FILE]    = "File",
    [MIBS_FSEK_LINK]    = "Link",
    [MIBS_FSEK_UNKNOWN] = "Unknown"
};

// https://learn.microsoft.com/en-us/windows/win32/sysinfo/retrieving-the-last-write-time
#ifdef MIBS_PLATFORM_WINDOWS

#define UNIX_TIME_START 0x019DB1DED53E8000
#define TICKS_PER_SEC   10000000

int64_t windows_system_time_to_unix(FILETIME ft)
{
    LARGE_INTEGER li;
    li.LowPart = ft.dwLowDateTime;
    li.HighPart = ft.dwHighDateTime;
    return (li.QuadPart - UNIX_TIME_START) / TICKS_PER_SEC;
}

int64_t windows_get_file_last_write(HANDLE hFile)
{
    FILETIME ftWrite;
    if (!GetFileTime(hFile, NULL, NULL, &ftWrite)) return -1;
    return windows_system_time_to_unix(ftWrite);
}

#endif

// https://learn.microsoft.com/en-us/windows/win32/procthread/creating-processes
int mibs_needs_rebuild(const char* out, const char* in)
{
#if defined(MIBS_PLATFORM_UNIX)
    struct stat statbuf;
    if (stat(out, &statbuf) < 0) {
        if (errno == ENOENT) return 1;
        mibs_log(MIBS_LL_ERROR, "could not stat %s: %s"MIBS_NL, out, strerror(errno));
        return -1;
    }
    int out_time = statbuf.st_mtime;
    if (stat(in, &statbuf) < 0) {
        mibs_log(MIBS_LL_ERROR, "could not stat %s: %s"MIBS_NL, in, strerror(errno));
        return -1;
    }
    int in_time = statbuf.st_mtime;
    return in_time > out_time;
    /* if (in_time > out_time) return 1; */
    /* return 0; */
#elif defined(MIBS_PLATFORM_WINDOWS)
    HANDLE hFileOut = CreateFile(out, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if (hFileOut == INVALID_HANDLE_VALUE) return 1;
    int64_t out_time = windows_get_file_last_write(hFileOut);
    if (out_time == -1) return -1;

    HANDLE hFileIn = CreateFile(in, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
    if (hFileIn == INVALID_HANDLE_VALUE) return -1;
    int64_t in_time = windows_get_file_last_write(hFileIn);
    if (in_time == -1) return -1;

    return in_time > out_time;
#else
#   error "Unimplemented platform"
#endif
}

bool mibs_check_int(const char* s)
{
    bool ok = false;
    const char* c = s;
    while (*c && isdigit(*c++)) ok = true;
    return ok;
}

bool mibs_compare_cstr(const char* lhs, const char* rhs)
{ 
    return strcmp(lhs, rhs) == 0;
}

const char* mibs_option_kind_to_cstr(int kind)
{
    switch (kind) {
    case MIBS_OV_STRING:  return "string";  break;
    case MIBS_OV_BOOLEAN: return "boolean"; break;
    case MIBS_OV_INTEGER: return "integer"; break;
    default: return "unknown kind"; break;
    }
}

bool mibs_expect_option_kind(Mibs_Option_Value* ov, const char* key, int kind)
{
    if ((int)ov->kind != kind) {
        mibs_log(MIBS_LL_ERROR, "option %s expects %s but got %s\n"MIBS_NL, key, mibs_option_kind_to_cstr(kind), mibs_option_kind_to_cstr(ov->kind));
        return false;
    }
    return true;
}

#endif // MIBS_IMPL

#endif // MIBS_H_
